mod closed_open;
mod closed;
mod general;
mod open_closed;
mod open;

use super::interval::Interval;

pub trait UnionOfIntervals<T>
where
    Self::IntervalType: Interval<T>,
    T: PartialOrd<T>,
{
    type IntervalType;
    fn is_empty(&self) -> bool;
    fn clear(&mut self);
    fn len(&self) -> usize;
    fn difference(&self, other: &Self) -> Self;
    fn symmetric_difference(&self, other: &Self) -> Self;
    fn intersection(&self, other: &Self) -> Self;
    fn union(&self, other: &Self) -> Self;
    fn contains_interval(&self, item: &Self::IntervalType) -> bool;
    fn contains_value(&self, value: &T) -> bool;
}

pub use self::closed_open::*;
pub use self::closed::*;
pub use self::general::*;
pub use self::open_closed::*;
pub use self::open::*;
