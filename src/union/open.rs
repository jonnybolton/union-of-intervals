use crate::interval::{Interval, OpenInterval};
use crate::union::UnionOfIntervals;

pub struct UnionOfIntervalsOpen<T> {
    intervals: Vec<OpenInterval<T>>,
}

impl<T> UnionOfIntervalsOpen<T> {
    pub fn new() -> Self {
        UnionOfIntervalsOpen {
            intervals: Vec::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        UnionOfIntervalsOpen {
            intervals: Vec::with_capacity(capacity),
        }
    }

    pub fn capacity(&self) -> usize {
        self.intervals.capacity()
    }

    pub fn reserve(&mut self, additional: usize) {
        self.intervals.reserve(additional);
    }

    pub fn reserve_exact(&mut self, additional: usize) {
        self.intervals.reserve_exact(additional);
    }

    pub fn shrink_to_fit(&mut self) {
        self.intervals.shrink_to_fit();
    }

    pub fn into_boxed_slice(self) -> Box<[OpenInterval<T>]> {
        self.intervals.into_boxed_slice()
    }

    pub fn truncate(&mut self, len: usize) {
        self.intervals.truncate(len);
    }

    #[inline]
    pub fn as_slice(&self) -> &[OpenInterval<T>] {
        self.intervals.as_slice()
    }

    #[inline]
    pub fn as_mut_slice(&mut self) -> &mut [OpenInterval<T>] {
        self.intervals.as_mut_slice()
    }

    #[inline]
    pub unsafe fn set_len(&mut self, new_len: usize) {
        self.intervals.set_len(new_len);
    }

    pub fn remove(&mut self, index: usize) -> OpenInterval<T> {
        self.intervals.remove(index)
    }
}

impl<T> UnionOfIntervals<T> for UnionOfIntervalsOpen<T>
where
    T: Ord,
{
    type IntervalType = OpenInterval<T>;

    fn is_empty(&self) -> bool {
        self.intervals.is_empty()
    }

    fn clear(&mut self) {
        self.intervals.clear()
    }

    fn len(&self) -> usize {
        self.intervals.len()
    }

    fn difference(&self, _other: &Self) -> Self {
        unimplemented!()
    }

    fn symmetric_difference(&self, _other: &Self) -> Self {
        unimplemented!()
    }

    fn intersection(&self, _other: &Self) -> Self {
        unimplemented!()
    }

    fn union(&self, _other: &Self) -> Self {
        //let mut union = Self::new();

        //let self_iter = self.intervals.iter();
        //let other_iter = other.intervals.iter();

        //self_iter.
        unimplemented!()
    }

    fn contains_interval(&self, item: &Self::IntervalType) -> bool {
        for interval in &self.intervals {
            if interval.contains_interval(item) {
                return true;
            }
        }
        false
    }

    fn contains_value(&self, value: &T) -> bool {
        for interval in &self.intervals {
            if interval.contains(value) {
                return true;
            }
        }
        false
    }
}
