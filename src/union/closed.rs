use std::fmt;
use crate::interval::{Interval, ClosedInterval};
use crate::union::UnionOfIntervals;

#[derive(Debug, Clone, PartialEq)]
pub struct UnionOfIntervalsClosed<T: Clone> {
    intervals: Vec<ClosedInterval<T>>,
}

impl<T: Clone + fmt::Display> fmt::Display for UnionOfIntervalsClosed<T> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self.intervals.len() {
            0 => write!(fmt, "∅"),
            _ => {
                let mut it = self.intervals.iter();

                write!(fmt, "{}", it.next().unwrap())?;

                for interval in it {
                    write!(fmt, " ∪ {}", interval)?;
                }

                Ok(())
            },
        }
    }
}

impl<T: Clone> UnionOfIntervalsClosed<T> {
    pub fn new() -> Self {
        UnionOfIntervalsClosed {
            intervals: Vec::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        UnionOfIntervalsClosed {
            intervals: Vec::with_capacity(capacity),
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.intervals.len()
    }

    pub fn capacity(&self) -> usize {
        self.intervals.capacity()
    }

    #[inline]
    pub fn push(&mut self, item: ClosedInterval<T>) {
        self.intervals.push(item);
    }

    pub fn reserve(&mut self, additional: usize) {
        self.intervals.reserve(additional);
    }

    pub fn reserve_exact(&mut self, additional: usize) {
        self.intervals.reserve_exact(additional);
    }

    pub fn shrink_to_fit(&mut self) {
        self.intervals.shrink_to_fit();
    }

    pub fn into_boxed_slice(self) -> Box<[ClosedInterval<T>]> {
        self.intervals.into_boxed_slice()
    }

    pub fn truncate(&mut self, len: usize) {
        self.intervals.truncate(len);
    }

    #[inline]
    pub fn as_slice(&self) -> &[ClosedInterval<T>] {
        self.intervals.as_slice()
    }

    #[inline]
    pub fn as_mut_slice(&mut self) -> &mut [ClosedInterval<T>] {
        self.intervals.as_mut_slice()
    }

    #[inline]
    pub unsafe fn set_len(&mut self, new_len: usize) {
        self.intervals.set_len(new_len);
    }

    pub fn remove(&mut self, index: usize) -> ClosedInterval<T> {
        self.intervals.remove(index)
    }
}

impl<T: PartialOrd<T> + Clone> UnionOfIntervals<T> for UnionOfIntervalsClosed<T> {
    type IntervalType = ClosedInterval<T>;

    fn is_empty(&self) -> bool {
        self.intervals.is_empty()
    }

    fn clear(&mut self) {
        self.intervals.clear()
    }

    fn len(&self) -> usize {
        self.intervals.len()
    }

    fn difference(&self, _other: &Self) -> Self {
        unimplemented!()
    }

    fn symmetric_difference(&self, _other: &Self) -> Self {
        unimplemented!()
    }

    fn intersection(&self, _other: &Self) -> Self {
        unimplemented!()
    }

    fn union(&self, other: &Self) -> Self {
        // TODO(jbolton): This is a very crude algorithm that will need to be optimized.

        let mut union = self.clone();

        'other: for other_interval in &other.intervals {
            let mut insert_index: usize = 0;
            let mut bounds = (
                other_interval.start_value().clone(),
                other_interval.end_value().clone()
            );

            let mut deletion_indices = Vec::<usize>::with_capacity(union.len());

            'union: for (i, union_interval) in union.intervals.iter().enumerate() {
                if other_interval.start_value() > union_interval.end_value() {
                    continue 'union;
                } else if other_interval.end_value() < union_interval.start_value() {
                    insert_index = i;
                    break 'union;
                } else {
                    if bounds.0 > *union_interval.start_value() {
                        bounds.0 = union_interval.start_value().clone();
                    }
                    if bounds.1 < *union_interval.end_value() {
                        bounds.1 = union_interval.end_value().clone();
                    }
                    deletion_indices.push(i);
                }
            }

            for i in deletion_indices.iter().rev() {
                union.intervals.remove(*i);
            }

            let interval = ClosedInterval {
                start: bounds.0.clone(),
                end:   bounds.1.clone(),
            };
            union.intervals.insert(insert_index, interval);
        }

        // TODO(jbolton): make shrinking optional?
        union.shrink_to_fit();

        union
    }

    fn contains_interval(&self, item: &Self::IntervalType) -> bool {
        for interval in &self.intervals {
            if interval.contains_interval(item) {
                return true;
            }
        }
        false
    }

    fn contains_value(&self, value: &T) -> bool {
        for interval in &self.intervals {
            if interval.contains(value) {
                return true;
            }
        }
        false
    }
}

impl<T> From<&[(T, T)]> for UnionOfIntervalsClosed<T>
where
    T: Clone,
{
    fn from(s: &[(T, T)]) -> Self {
        let mut intervals = Vec::<ClosedInterval<T>>::with_capacity(s.len());
        for pair in s {
            intervals.push(ClosedInterval::from(pair));
        }
        Self {
            intervals: intervals,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::UnionOfIntervalsClosed as Union;
    use crate::union::UnionOfIntervals;

    #[test]
    fn test_union() {
        let empty: Union<f32> = Union::<f32>::new();
        let a: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(0.0, 1.0)]));
        let b: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(1.0, 2.0)]));
        let c: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(0.0, 2.0)]));
        let d: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(0.5, 1.5)]));
        let e: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(3.0, 4.0)]));
        let f: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(4.5, 5.0)]));
        let g: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(0.0, 1.0), (3.0, 4.0), (4.5, 5.0)]));
        let h: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(1.0, 2.0), (2.5, 3.5)]));
        let i: Union<f32> = Union::from(<&[(f32, f32)]>::from(&[(0.0, 2.0), (2.5, 4.0), (4.5, 5.0)]));

        assert_eq!(empty.union(&empty), empty);
        assert_eq!(empty.union(&a), a);
        assert_eq!(a.union(&empty), a);
        assert_eq!(a.union(&b), c);
        assert_eq!(b.union(&a), c);
        assert_eq!(c.union(&d), c);
        assert_eq!(d.union(&c), c);
        assert_eq!(g.union(&h), i);
        assert_eq!(h.union(&g), i);
    }
}