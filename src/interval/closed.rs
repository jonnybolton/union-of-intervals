use std::fmt;

use crate::interval::Interval;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct ClosedInterval<T: Clone> {
    pub start: T,
    pub end: T,
}

impl<T: fmt::Display + Clone> fmt::Display for ClosedInterval<T> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "[{}, {}]", self.start, self.end)
    }
}

impl<T: Clone> ClosedInterval<T> {
    pub fn new(start: T, end: T) -> Self {
        ClosedInterval {
            start: start,
            end: end,
        }
    }
}

impl<T: PartialOrd + Clone> Interval<T> for ClosedInterval<T> {
    fn start_value(&self) -> &T {
        &self.start
    }

    fn end_value(&self) -> &T {
        &self.end
    }

    fn contains(&self, item: &T) -> bool
    {
        (self.start <= *item) && (self.end >= *item)
    }

    fn contains_interval(&self, item: &Self) -> bool
    {
        (self.start <= item.start) && (self.end >= item.end)
    }

    fn is_disjoint_from(&self, other: &Self) -> bool
    {
        (self.start > other.end) || (self.end < other.start)
    }
}

impl<T: Clone> From<(T, T)> for ClosedInterval<T> {
    fn from(pair: (T, T)) -> Self {
        ClosedInterval {
            start: pair.0,
            end: pair.1,
        }
    }
}

impl<T: Clone> From<&(T, T)> for ClosedInterval<T> {
    fn from(pair: &(T, T)) -> Self {
        ClosedInterval {
            start: pair.0.clone(),
            end: pair.1.clone(),
        }
    }
}

impl<T: Clone> From<&mut (T, T)> for ClosedInterval<T> {
    fn from(pair: &mut (T, T)) -> Self {
        ClosedInterval {
            start: pair.0.clone(),
            end: pair.1.clone(),
        }
    }
}
