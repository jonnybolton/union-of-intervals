use std::fmt;

use crate::interval::Interval;

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
pub struct ClosedOpenInterval<T> {
    start: T,
    end: T,
}

impl<T> fmt::Debug for ClosedOpenInterval<T>
where
    T: fmt::Debug
{
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "[{:?}, {:?})", self.start, self.end)
    }
}

impl<T> Interval<T> for ClosedOpenInterval<T>
where
    T: Ord,
{
    fn start_value(&self) -> &T {
        &self.start
    }

    fn end_value(&self) -> &T {
        &self.end
    }

    fn contains(&self, item: &T) -> bool
    {
        (self.start <= *item) && (self.end > *item)
    }

    fn contains_interval(&self, item: &Self) -> bool
    {
        (self.start <= item.start) && (self.end >= item.end)
    }

    fn is_disjoint_from(&self, other: &Self) -> bool
    {
        (self.start >= other.end) || (self.end <= other.start)
    }
}
