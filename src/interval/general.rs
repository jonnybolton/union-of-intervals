use std::fmt;
use std::cmp::Ordering;

use crate::interval::Interval;

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
pub enum BoundType {
    Inclusive,
    Exclusive,
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
pub struct Bound<T> {
    value: T,
    bound_type: BoundType,
}

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
pub struct GeneralInterval<T> {
    start: Bound<T>,
    end: Bound<T>,
}

impl<T> fmt::Debug for GeneralInterval<T>
where
    T: fmt::Debug
{
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let start_brace = match self.start.bound_type {
            BoundType::Inclusive => String::from("["),
            BoundType::Exclusive => String::from("("),
        };
        let end_brace = match self.end.bound_type {
            BoundType::Inclusive => String::from("]"),
            BoundType::Exclusive => String::from(")"),
        };
        write!(fmt, "{}{:?}, {:?}{}", start_brace, self.start.value, self.end.value, end_brace)
    }
}

impl<T> Interval<T> for GeneralInterval<T>
where
    T: Ord,
{
    fn start_value(&self) -> &T {
        &self.start.value
    }

    fn end_value(&self) -> &T {
        &self.end.value
    }

    fn contains(&self, item: &T) -> bool
    {
        let is_above_lower_bound = match self.start.value.cmp(item) {
            Ordering::Less => true,
            Ordering::Greater => false,
            Ordering::Equal => (self.start.bound_type == BoundType::Inclusive),
        };
        let is_below_upper_bound = match self.end.value.cmp(item) {
            Ordering::Less => false,
            Ordering::Greater => true,
            Ordering::Equal => (self.start.bound_type == BoundType::Inclusive),
        };
        (is_above_lower_bound && is_below_upper_bound)
    }

    fn contains_interval(&self, item: &Self) -> bool
    {
        let is_above_lower_bound =
            Self::compare_lower_bounds(&self.start, &item.start) != Ordering::Greater;
        let is_below_upper_bound =
            Self::compare_upper_bounds(&self.end, &item.end) != Ordering::Less;
        (is_above_lower_bound && is_below_upper_bound)
    }

    fn is_disjoint_from(&self, other: &Self) -> bool
    {
        let is_strictly_greater =
            Self::compare_lower_and_upper_bounds(&self.start, &other.end) == Ordering::Greater;
        let is_strictly_less =
            Self::compare_lower_and_upper_bounds(&other.start, &self.end) == Ordering::Greater;
        (is_strictly_greater || is_strictly_less)
    }
}

impl<T> GeneralInterval<T>
where
    T: Ord,
{
    fn compare_lower_bounds(lhs: &Bound<T>, rhs: &Bound<T>) -> Ordering {
        match lhs.value.cmp(&rhs.value) {
            Ordering::Equal => match (lhs.bound_type, rhs.bound_type) {
                (BoundType::Inclusive, BoundType::Exclusive) => Ordering::Less,
                (BoundType::Exclusive, BoundType::Inclusive) => Ordering::Greater,
                _ => Ordering::Equal,
            },
            ordering => ordering,
        }
    }

    fn compare_upper_bounds(lhs: &Bound<T>, rhs: &Bound<T>) -> Ordering {
        match lhs.value.cmp(&rhs.value) {
            Ordering::Equal => match (lhs.bound_type, rhs.bound_type) {
                (BoundType::Inclusive, BoundType::Exclusive) => Ordering::Greater,
                (BoundType::Exclusive, BoundType::Inclusive) => Ordering::Less,
                _ => Ordering::Equal,
            },
            ordering => ordering,
        }
    }

    fn compare_lower_and_upper_bounds(lower: &Bound<T>, upper: &Bound<T>) -> Ordering {
        match lower.value.cmp(&upper.value) {
            Ordering::Equal => match (lower.bound_type, upper.bound_type) {
                (BoundType::Inclusive, BoundType::Inclusive) => Ordering::Equal,
                _ => Ordering::Greater,
            },
            ordering => ordering,
        }
    }
}
