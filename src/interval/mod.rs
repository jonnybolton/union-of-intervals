mod closed_open;
mod closed;
mod general;
mod open_closed;
mod open;

pub trait Interval<T: PartialOrd<T>>: PartialEq {
    fn start_value(&self) -> &T;
    fn end_value(&self) -> &T;
    fn contains(&self, item: &T) -> bool;
    fn contains_interval(&self, item: &Self) -> bool;
    fn is_disjoint_from(&self, other: &Self) -> bool;
}

pub use self::closed_open::*;
pub use self::closed::*;
pub use self::general::*;
pub use self::open_closed::*;
pub use self::open::*;
